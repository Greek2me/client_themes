@@@@@@@@@@@@@@@@@@@@@@@@@@@
Client_Themes Documentation
@@@@@@@@@@@@@@@@@@@@@@@@@@@

Introducing a simple, easy-to-use theme system for Blockland. Client_Themes uses the existing add-on system to allow for simple theme installation. This README file explains the basics of both using Client_Themes and creating themes for it.

.. sectnum::
.. contents:: Table of Contents
	:backlinks: top

Usage
=====

Installation
------------

Install Client_Themes.zip to your "Add-Ons" folder, along with any "Theme_*.zip" files.

Select a Theme
--------------

To change the theme, navigate to the Blockland Options menu and click the "Theme" button under the "GUI Settings" panel.

Theme Developers
================

Creating themes for use with Client_Themes is extremely simple.

Replacement Images
------------------

To replace a default bitmap, place a file of the same name in your Theme_Whatever.zip file.

Description.txt - The Information File
--------------------------------------

Each theme add-on that will work with Client_Themes must contain a text file called "description.txt". This file contains information about the title and author.

Here is an example file::

	Title: My Theme
	Author: Greek2me

An additional description can be included below the title and author.

Custom Profiles
---------------

Client_Themes supports the replacement of default Blockland GUI profiles with custom ones. To use custom profiles, include a file in your theme called "profiles.cs". This file should contain only GuiControlProfiles.

Custom profiles must follow strict naming conventions. Profile names are structured like this::

	Theme_MyThemeName__OriginalProfileName

Note that there are two underscores between "MyThemeName" and "OriginalProfileName".

GUI "Tweaks"
------------

If you need to make changes to the GUI, such as repositioning or adding elements, do so by creating a file called "tweaks.cs" in your theme package. This file can contain all necessary changes. It will be loaded after the theme images have been applied.

About Client_Themes
===================

Created by Greek2me. (BLID 11902)

Contact
-------

 - `Website <http://greek2me.us>`_
 - `Blockland Forums <http://forum.blockland.us/index.php?action=profile;u=22331>`_
 - `Email <greektume@gmail.com>`_
